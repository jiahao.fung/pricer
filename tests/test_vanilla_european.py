import unittest
from datetime import datetime

from pricer.interface.pricer_params import BlackScholesPricer, BlackPricer
from pricer.interface.pricing_engine import option_calculator
from pricer.interface.pricing_environment import PricingEnvironment
from pricer.interface.pricing_measure import PricingMeasure
from pricer.market_data.curve.const_curve import ConstantCurve
from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.holidays.calendar import Calendar
from pricer.market_data.mkt_data_util import DayCountConvention
from pricer.market_data.surface.const_surface import ConstantVolSurface
from pricer.product.option.vanilla_option import VanillaEuropean
from pricer.product.product_util import OptionType
from pricer.product.security import EquityStock


class TestVanillaEuropean(unittest.TestCase):
    def setUp(self):
        strike = 100
        val_date = datetime(2021, 2, 1)
        expiration_date = datetime(2022, 2, 1)
        delivery_date = datetime(2022, 2, 1)
        self.call = VanillaEuropean(EquityStock("0001.HK"),
                                    OptionType.Call,
                                    strike,
                                    expiration_date,
                                    delivery_date)
        self.put = VanillaEuropean(EquityStock("0001.HK"),
                                   OptionType.Put,
                                   strike,
                                   expiration_date,
                                   delivery_date)
        bs = BusinessTime(Calendar.load_act365_calendar(), DayCountConvention.ACT365)
        discounting_curve = ConstantCurve(0.025, val_date)
        dividend_curve = ConstantCurve(0., val_date)
        vol_surface = ConstantVolSurface(0.25, val_date, bs)
        bs_pricer = BlackScholesPricer()
        black_pricer = BlackPricer()
        spot_price = 100.
        self.bs_env = PricingEnvironment(spot_price, vol_surface, discounting_curve, dividend_curve,
                                         val_date, bs_pricer)
        self.black_env = PricingEnvironment(spot_price, vol_surface, discounting_curve, dividend_curve,
                                            val_date, black_pricer)

    def test_black_scholes_pricer(self):
        call_price = option_calculator(PricingMeasure.PV,
                                       self.call,
                                       self.bs_env)
        put_price = option_calculator(PricingMeasure.PV,
                                      self.put,
                                      self.bs_env)
        self.assertAlmostEqual(call_price, 11.108170269826182, 6), "BS Call Option Pricing Test Failed"
        self.assertAlmostEqual(put_price, 8.639161472659445, 6), "BS Put Option Pricing Test Failed"

    def test_black_pricer(self):
        black_scholes_call = option_calculator(PricingMeasure.PV,
                                               self.call,
                                               self.bs_env)
        black_scholes_put = option_calculator(PricingMeasure.PV,
                                              self.put,
                                              self.bs_env)
        black_call_price = option_calculator(PricingMeasure.PV,
                                             self.call,
                                             self.black_env)
        black_put_price = option_calculator(PricingMeasure.PV,
                                            self.put,
                                            self.black_env)
        self.assertAlmostEqual(black_scholes_call, black_call_price, 6), "BS Call Option Pricing Test Failed"
        self.assertAlmostEqual(black_scholes_put, black_put_price, 6), "BS Put Option Pricing Test Failed"
