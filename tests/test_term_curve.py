import numpy as np
import unittest
from datetime import datetime, timedelta
from math import exp

from pricer.market_data.curve.term_curve import TermCurve
from pricer.market_data.mkt_data_util import DayCountConvention


class TestTermCurve(unittest.TestCase):
    def setUp(self):
        self.day_count = DayCountConvention.ACT365
        self.val_date = datetime(2024, 5, 17)
        self.spot_rates = [0.025, 0.022, 0.02]
        self.spot_dates = [datetime(2024, 6, 17), datetime(2024, 8, 17), datetime(2024, 11, 17)]
        self.curve = TermCurve(self.val_date, self.spot_dates, self.spot_rates, DayCountConvention.ACT365)
        self.taus = [(spot_date - self.val_date).days / self.day_count.value for spot_date in self.spot_dates]
        self.test_dates = [datetime(2024, 5, 19),
                           datetime(2024, 7, 17),
                           datetime(2024, 10, 17),
                           datetime(2024, 12, 1)]

    def test_curve(self):
        for test_date in self.test_dates:
            if test_date < min(self.spot_dates):
                self.assertAlmostEqual(self.curve.zero_rate(test_date), self.spot_rates[0])
                tau = (test_date - self.val_date).days / self.day_count.value
                self.assertAlmostEqual(self.curve.discount(test_date), exp(-self.spot_rates[0] * tau))
            elif test_date > max(self.spot_dates):
                self.assertAlmostEqual(self.curve.zero_rate(test_date), self.spot_rates[-1])
                tau = (test_date - self.val_date).days / self.day_count.value
                self.assertAlmostEqual(self.curve.discount(test_date), exp(-self.spot_rates[-1] * tau))
            else:
                tau = (test_date - self.val_date).days / self.day_count.value
                rate = np.interp(tau, self.taus, self.spot_rates)
                self.assertAlmostEqual(self.curve.zero_rate(test_date), rate)
                self.assertAlmostEqual(self.curve.discount(test_date), exp(-tau * rate))

    def test_curve_bump(self):
        bumped_curve = self.curve.bump(0.01)
        for test_date in self.test_dates:
            self.assertAlmostEqual(self.curve.zero_rate(test_date) + 0.01, bumped_curve.zero_rate(test_date))

    def test_curve_rolling(self):
        new_valuation_date = self.val_date + timedelta(days=1)
        rolled_curve = self.curve.rolling(new_valuation_date)
        rolled_curve_outer = TermCurve(new_valuation_date, self.spot_dates, self.spot_rates, DayCountConvention.ACT365)
        for dt in self.test_dates:
            self.assertAlmostEqual(rolled_curve.discount(dt), rolled_curve_outer.discount(dt))
