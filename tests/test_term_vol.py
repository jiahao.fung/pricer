import pickle
import unittest
import numpy as np
from datetime import datetime, timedelta
from math import sqrt

from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.holidays.calendar import Calendar
from pricer.market_data.mkt_data_util import DayCountConvention
from pricer.market_data.surface.term_curve import TermVolSurface


class TestTermVolSurface(unittest.TestCase):
    def setUp(self):

        with open("./tests/holidays/ch.pickle", 'rb') as f:
            holidays = pickle.load(f)
            holidays = pickle.loads(holidays)
        self.calendar = Calendar(holidays)
        self.business_time = BusinessTime(self.calendar, DayCountConvention.BUZ244)

        self.val_date = datetime(2024, 5, 17)
        self.vols = [0.35, 0.3, 0.25, 0.25]
        self.exp_dates = [datetime(2024, 6, 17),
                          datetime(2024, 8, 16),
                          datetime(2024, 11, 18),
                          datetime(2024, 12, 17)]
        self.surface = TermVolSurface(self.val_date, self.vols, self.exp_dates, self.business_time)

        self.test_dates = [datetime(2024, 5, 21),
                           datetime(2024, 7, 17),
                           datetime(2024, 10, 17),
                           datetime(2024, 12, 1)]
        self.taus = [self.business_time.compute_year_fraction(self.val_date, exp_date) for exp_date in self.exp_dates]
        self.total_variances = []
        for i in range(len(self.taus)):
            self.total_variances.append(self.taus[i] * self.vols[i] * self.vols[i])

    def test_surface(self):
        for test_date in self.test_dates:
            if test_date < min(self.exp_dates):
                self.assertAlmostEqual(self.surface.volatility(test_date), self.vols[0])
            elif test_date < max(self.exp_dates):
                tau = self.business_time.compute_year_fraction(self.surface.get_val_date(), test_date)
                variances = np.interp(tau, self.taus, self.total_variances)
                vol = sqrt(variances / tau)
                self.assertAlmostEqual(self.surface.volatility(test_date), vol)
            else:
                self.assertAlmostEqual(self.surface.volatility(test_date), self.vols[-1])

    def test_surface_relative_bump(self):
        bump_size = 0.2
        bumped_surface = self.surface.relative_bump(bump_size)
        for test_date in self.test_dates:
            self.assertAlmostEqual(self.surface.volatility(test_date) * (1+bump_size),
                                   bumped_surface.volatility(test_date))

    def test_surface_bump(self):
        bump_size = 0.05
        bumped_surface = self.surface.bump(bump_size)
        bumped_variances = []
        for i in range(len(self.vols)):
            bumped_variances.append((self.vols[i] + bump_size) ** 2 * self.taus[i])
        for test_date in self.test_dates:
            if test_date < min(self.exp_dates) or test_date > max(self.exp_dates):
                self.assertAlmostEqual(self.surface.volatility(test_date) + bump_size,
                                       bumped_surface.volatility(test_date))
            else:
                tau = self.business_time.compute_year_fraction(self.surface.get_val_date(), test_date)
                variances = np.interp(tau, self.taus, bumped_variances)
                vol = sqrt(variances / tau)
                self.assertAlmostEqual(bumped_surface.volatility(test_date), vol)

    def test_surface_rolling(self):
        new_val_date = self.val_date
        while True:
            new_val_date = new_val_date + timedelta(days=1)
            if not self.calendar.is_holiday(new_val_date):
                break
        rolled_curve = self.surface.rolling(new_val_date)
        cutoff_variances = self.surface.variance(new_val_date)
        for test_date in self.test_dates:
            self.assertAlmostEqual(self.surface.variance(test_date) - cutoff_variances,
                                   rolled_curve.variance(test_date))
