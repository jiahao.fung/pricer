import pickle
import unittest
from datetime import datetime

from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.holidays.calendar import Calendar
from pricer.market_data.mkt_data_util import DayCountConvention


class TestHolidays(unittest.TestCase):
    def setUp(self):
        with open("./tests/holidays/ch.pickle", 'rb') as f:
            holidays = pickle.load(f)
            holidays = pickle.loads(holidays)
        self.calendar = Calendar(holidays)
        self.business_time = BusinessTime(self.calendar, DayCountConvention.BUZ244)

    def test_calendar(self):
        self.assertFalse(self.calendar.is_holiday(datetime(2023, 10, 9)))

    def test_business_time(self):
        self.assertAlmostEqual(241. / 244., self.business_time.compute_year_fraction(
            datetime(2023, 1, 3), datetime(2023, 12, 29)))
