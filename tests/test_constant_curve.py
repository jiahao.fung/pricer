import unittest
from datetime import datetime
from math import exp

from pricer.market_data.curve.const_curve import ConstantCurve
from pricer.market_data.mkt_data_util import DayCountConvention


class TestConstantVolSurface(unittest.TestCase):
    def setUp(self):
        self.rate = 0.025
        self.val_date = datetime(2024, 5, 16)
        self.curve = ConstantCurve(self.rate, self.val_date, DayCountConvention.ACT365)

    def test_curve(self):
        exp_dates = [datetime(2024, 8, 16), datetime(2024, 11, 20),
                     datetime(2025, 1, 1 ), datetime(2026, 1, 1)]
        taus = [(exp_date - self.val_date).days / 365 for exp_date in exp_dates]
        for i in range(len(exp_dates)):
            self.assertAlmostEqual(0.025, self.curve.zero_rate(exp_dates[i]))
            self.assertAlmostEqual(exp(-self.rate * taus[i]), self.curve.discount(exp_dates[i]))

    def test_curve_bump(self):
        bumped_curve = self.curve.bump(0.005)
        self.assertAlmostEqual(0.03, bumped_curve.zero_rate(datetime(2025, 1, 1)))

    def test_curve_rolling(self):
        rolled_date = datetime(2024, 5, 17)
        rolled_curve = self.curve.rolling(rolled_date)
        self.assertAlmostEqual(self.rate, rolled_curve.zero_rate(datetime(2025, 1, 1)))
        rolled_tau = (datetime(2025, 1, 1) - rolled_date).days / 365.
        self.assertAlmostEqual(exp(-self.rate * rolled_tau), rolled_curve.discount(datetime(2025, 1, 1)))
