import unittest
from datetime import datetime

from pricer.Solver.implied_vol_solver import ImpliedVolSolver
from pricer.interface.pricer_params import BlackScholesPricer
from pricer.interface.pricing_environment import PricingEnvironment
from pricer.market_data.curve.const_curve import ConstantCurve
from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.holidays.calendar import Calendar
from pricer.market_data.mkt_data_util import DayCountConvention
from pricer.market_data.surface.const_surface import ConstantVolSurface
from pricer.product.ledger import PriceLedger
from pricer.product.option.listed_option import ListedVanillaEuropean
from pricer.product.product_util import OptionType, MktPriceType
from pricer.product.security import EquityStock


class TestVanillaEuropean(unittest.TestCase):
    def setUp(self):
        strike = 100
        val_date = datetime(2021, 2, 1)
        expiration_date = datetime(2022, 2, 1)
        delivery_date = datetime(2022, 2, 1)
        call_price_ledger = PriceLedger(settle=11.108170269826182)
        put_price_ledger = PriceLedger(settle=8.639161472659445)
        underlying = EquityStock("0001.HK")
        self.call = ListedVanillaEuropean(call_price_ledger, expiration_date, underlying,
                                          OptionType.Call, strike, delivery_date)
        self.put = ListedVanillaEuropean(put_price_ledger, expiration_date, underlying,
                                         OptionType.Put, strike, delivery_date)
        bs = BusinessTime(Calendar.load_act365_calendar(), DayCountConvention.ACT365)
        discounting_curve = ConstantCurve(0.025, val_date)
        dividend_curve = ConstantCurve(0., val_date)
        vol_surface = ConstantVolSurface(0.25, val_date, bs)
        bs_pricer = BlackScholesPricer()
        spot_price = 100.
        self.bs_env = PricingEnvironment(spot_price, vol_surface, discounting_curve, dividend_curve,
                                         val_date, bs_pricer)
        self.solver = ImpliedVolSolver([self.call, self.put], self.bs_env)

    def test_black_scholes_pricer(self):
        iv_dict = self.solver.solve_implied_vol(MktPriceType.Settle)
        self.assertAlmostEqual(0.25, iv_dict[self.call.expiration_date][self.call.strike], 8)
