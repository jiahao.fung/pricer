from pricer.interface.pricer.black_scholes import vanilla_european_pricer
from pricer.interface.pricing_environment import PricingEnvironment
from pricer.interface.pricing_measure import PricingMeasure
from pricer.product.instrument import Instrument
from pricer.product.option.vanilla_option import VanillaOption
from pricer.product.product_util import EuropeanOption


def option_calculator(request: PricingMeasure,
                      option: Instrument,
                      env: PricingEnvironment):
    if isinstance(option, VanillaOption) and isinstance(option, EuropeanOption):
        return vanilla_european_pricer(request, option, env.spot_price, env.vol_surface, env.discounting_curve,
                                       env.dividend_curve, env.valuation_date, env.pricer)
    else:
        raise ValueError("Wrong Option Type @ option_calculator")
