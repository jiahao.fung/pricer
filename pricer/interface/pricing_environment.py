from datetime import datetime

from pricer.interface.pricer_params import Pricer
from pricer.market_data.curve.curve import Curve
from pricer.market_data.surface.surface import Surface


class PricingEnvironment(object):
    def __init__(self,
                 spot_price: float,
                 vol_surface: Surface,
                 discounting_curve: Curve,
                 dividend_curve: Curve,
                 val_date: datetime,
                 pricer: Pricer):
        self._spot_price = spot_price
        self._vol_surface = vol_surface
        self._discounting_curve = discounting_curve
        self._dividend_curve = dividend_curve
        self._val_date = val_date
        self._pricer = pricer

    @property
    def spot_price(self) -> float:
        return self._spot_price

    @property
    def vol_surface(self) -> Surface:
        return self._vol_surface

    @property
    def discounting_curve(self) -> Curve:
        return self._discounting_curve

    @property
    def dividend_curve(self) -> Curve:
        return self._dividend_curve

    @property
    def valuation_date(self) -> datetime:
        return self._val_date

    @property
    def pricer(self) -> Pricer:
        return self._pricer

    @spot_price.setter
    def spot_price(self, spot_price_):
        self._spot_price = spot_price_

    @vol_surface.setter
    def vol_surface(self, vol_surface_):
        self._vol_surface = vol_surface_

    @discounting_curve.setter
    def discounting_curve(self, discounting_curve_):
        self._discounting_curve = discounting_curve_

    @dividend_curve.setter
    def dividend_curve(self, dividend_curve_):
        self._dividend_curve = dividend_curve_

    @valuation_date.setter
    def valuation_date(self, valuation_date):
        self._val_date = valuation_date

    @pricer.setter
    def pricer(self, pricer_):
        self._pricer = pricer_
