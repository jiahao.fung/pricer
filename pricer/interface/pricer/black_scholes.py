from datetime import datetime
from math import log

from pricer.interface.pricer_params import Pricer, BlackScholesPricer, BlackPricer
from pricer.interface.pricing_measure import PricingMeasure
from pricer.market_data.curve.curve import Curve
from pricer.market_data.surface.surface import Surface
from pricer.numerics.analytical.black import BlackVanillaEuropean
from pricer.numerics.analytical.black_scholes import BSVanillaEuropean
from pricer.product.option.vanilla_option import VanillaOption


def vanilla_european_pricer(request: PricingMeasure,
                            option: VanillaOption,
                            spot_price,
                            vol_surface: Surface,
                            discounting_curve: Curve,
                            dividend_curve: Curve,
                            val_date: datetime,
                            pricer: Pricer
                            ):
    strike = option.strike
    option_type = option.option_type
    expiration_date = option.expiration_date
    delivery_date = option.delivery_date

    vol = vol_surface.volatility(expiration_date)
    buz_tau = vol_surface.get_business_time().compute_year_fraction(val_date, expiration_date)
    buz_tau_delivery = vol_surface.get_business_time().compute_year_fraction(val_date, delivery_date)

    dfr_exp = discounting_curve.discount(expiration_date)
    dfr_delivery = discounting_curve.discount(delivery_date)
    dfq_exp = dividend_curve.discount(expiration_date)

    r = -log(dfr_exp) / buz_tau
    r_delivery = -log(dfr_delivery) / buz_tau_delivery
    q = -log(dfq_exp) / buz_tau
    if isinstance(pricer, BlackScholesPricer):
        if request == PricingMeasure.PV:
            return BSVanillaEuropean.pv(spot_price, strike, vol, r, q, buz_tau,
                                        option_type, r_delivery, buz_tau_delivery)
        else:
            raise ValueError("Wrong Request for vanilla_european_pricer")

    elif isinstance(pricer, BlackPricer):
        forward_price = spot_price / dfr_exp
        if request == PricingMeasure.PV:
            return BlackVanillaEuropean.pv(forward_price, strike, vol, r, buz_tau,
                                           option_type, r_delivery, buz_tau_delivery)
        else:
            raise ValueError("Wrong Request for vanilla_european_pricer")

    else:
        raise ValueError("Wrong Pricer type for vanilla european pricer")
