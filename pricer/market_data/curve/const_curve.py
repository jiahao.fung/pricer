from abc import ABC
from datetime import datetime
from math import exp

from pricer.market_data.curve.curve import Curve
from pricer.market_data.mkt_data_util import DayCountConvention


class ConstantCurve(Curve, ABC):
    def __init__(self, rate: float, val_date: datetime,
                 day_count: DayCountConvention = DayCountConvention.ACT365):
        self._rate = rate
        self._val_date = val_date
        self._day_count = day_count

    def discount(self, val_date: datetime):
        if val_date < self._val_date:
            raise ValueError("Class Constant Curve: Discount factor should be calculated in the future")
        tau = (val_date - self._val_date).days / self._day_count.ACT365.value
        return exp(-self._rate * tau)

    def zero_rate(self, val_date: datetime):
        if val_date < self._val_date:
            raise ValueError("Class Constant Curve: Zero rate should be calculated in the future")
        return self._rate

    def rolling(self, new_valuation_date: datetime):
        return ConstantCurve(self._rate, new_valuation_date)

    def bump(self, bump_size: float):
        return ConstantCurve(self._rate + bump_size, self._val_date)
