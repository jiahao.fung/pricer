from abc import ABC, abstractmethod
from datetime import datetime


class Curve(ABC):
    @abstractmethod
    def discount(self, val_date: datetime):
        raise NotImplemented

    @abstractmethod
    def zero_rate(self, val_date: datetime):
        raise NotImplemented

    @abstractmethod
    def bump(self, bump_size: float):
        raise NotImplemented

    @abstractmethod
    def rolling(self, new_valuation_date: datetime):
        raise NotImplemented
