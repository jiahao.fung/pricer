from abc import ABC
from copy import deepcopy
from datetime import datetime
from math import exp
from scipy.interpolate import interp1d
from typing import List


from pricer.market_data.curve.curve import Curve
from pricer.market_data.dt_utils import DateUtils
from pricer.market_data.mkt_data_util import DayCountConvention, REUtils


class TermCurve(Curve, ABC):
    def __init__(self,
                 val_date: datetime,
                 spot_dates: List[datetime],
                 spot_rates: List[float],
                 day_count: DayCountConvention = DayCountConvention.ACT365):
        self._val_date = val_date
        self._day_count = day_count
        self._spot_dates = spot_dates
        self._spot_rates = spot_rates
        taus = [(spot_date - val_date).days / self._day_count.value for spot_date in self._spot_dates]

        xs = deepcopy(taus)
        xs.insert(0, 0.)
        ys = deepcopy(spot_rates)
        ys.insert(0, spot_rates[0])
        self._interp = interp1d(x=xs, y=ys, kind='linear', bounds_error=False, assume_sorted=False,
                                fill_value=self._spot_rates[-1])

    @classmethod
    def load_term_curve_from_dict(cls,
                                  val_date: datetime,
                                  curve_dict: dict,
                                  day_count: DayCountConvention = DayCountConvention.ACT365):
        spot_dates = []
        spot_rates = []
        for key, value in curve_dict.items():
            spot_dates.append(key)
            spot_rates.append(value)
        return cls(val_date, spot_dates, spot_rates, day_count)

    @classmethod
    def load_term_curve_from_tenor(cls,
                                   val_date: datetime,
                                   spot_tenors: List[str],
                                   spot_rates: List[float],
                                   day_count: DayCountConvention = DayCountConvention.ACT365):
        spot_dates = []
        try:
            for tenor in spot_tenors:
                period, time_unit = REUtils.separate_alpha_num(tenor)
                spot_dates.append(DateUtils.calculate_date_from_tenor(val_date, period, time_unit))
        except Exception as e:
            print(f"Class Term Curve: Error occurred when calculate spot_dates, {e}")
        return cls(val_date, spot_dates, spot_rates, day_count)

    def discount(self, val_date: datetime):
        if val_date < self._val_date:
            raise ValueError("Class Term Curve: Discount factor should be calculated in the future")
        rate_ = self.zero_rate(val_date)
        tau_ = (val_date - self._val_date).days / self._day_count.value
        return exp(-rate_ * tau_)

    def zero_rate(self, val_date: datetime):
        if val_date < self._val_date:
            raise ValueError("Class Term Curve: Zero rate should be calculated in the future")
        tau_ = (val_date - self._val_date).days / self._day_count.value
        return float(self._interp(tau_))

    def bump(self, bump_size: float):
        return TermCurve(self._val_date,
                         self._spot_dates,
                         [rate + bump_size for rate in self._spot_rates],
                         self._day_count)

    def rolling(self, new_valuation_date: datetime):
        curve_dict = {}
        for i in range(len(self._spot_dates)):
            if self._spot_dates[i] >= new_valuation_date:
                curve_dict[self._spot_dates[i]] = self._spot_rates[i]
        return TermCurve.load_term_curve_from_dict(new_valuation_date, curve_dict, self._day_count)
