from datetime import datetime
from dateutil.relativedelta import relativedelta


class DateUtils:
    @staticmethod
    def calculate_date_from_tenor(
            val_date: datetime,
            period: int,
            time_unit: str
    ):
        if time_unit == "d" or time_unit == 'D':
            return val_date + relativedelta(days=period)
        elif time_unit == "w" or time_unit == "W":
            return val_date + relativedelta(weeks=period)
        elif time_unit == "m" or time_unit == "M":
            return val_date + relativedelta(months=period)
        elif time_unit == "y" or time_unit == "Y":
            return val_date + relativedelta(years=period)
        else:
            raise ValueError(f"DateUtils->calculate_date_from_tenor(): Unrecognized time unit: {time_unit}")
