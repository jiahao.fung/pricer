import re
from enum import Enum


class DayCountConvention(Enum):
    ACT365 = 365
    ACT360 = 360
    BUZ244 = 244
    BUZ252 = 252


class REUtils:
    @staticmethod
    def separate_alpha_num(str_comb: str):
        alpha_part = re.findall('[a-zA-Z]+', str_comb)
        num_part = re.findall('[0-9]+', str_comb)
        return num_part, alpha_part
