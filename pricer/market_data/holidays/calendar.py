from datetime import datetime
from typing import List


class Calendar(object):
    def __init__(self,
                 holidays):
        self._holidays = holidays

    @classmethod
    def load_act365_calendar(cls):
        return cls(holidays=None)

    @classmethod
    def load_calendar_from_str_list(cls, holidays):
        """
        :param holidays: ['%Y-%m-%d']
        :return:
        """
        dt_holidays = [datetime.strptime(x, "%Y-%m-%d") for x in holidays]
        return cls(dt_holidays)

    def is_holiday(self, val_date: datetime) -> bool:
        if self._holidays is None:
            return False
        else:
            return val_date.weekday() in [5, 6] or val_date in self._holidays
