from datetime import datetime, timedelta
from pricer.market_data.holidays.calendar import Calendar
from pricer.market_data.mkt_data_util import DayCountConvention


class BusinessTime(object):
    def __init__(self,
                 calendar: Calendar,
                 day_count: DayCountConvention):
        self._calendar = calendar
        self._days_in_year = day_count.value

    @property
    def days_in_year(self) -> int:
        return self._days_in_year

    def compute_days_interval(self,
                              start: datetime,
                              end: datetime,
                              include_start: bool = False,
                              include_end: bool = True):
        if start > end:
            return -self.compute_days_interval(end, start, include_start, include_end)
        count = 0
        dt = start
        while dt < end:
            dt += timedelta(days=1)
            if not self._calendar.is_holiday(dt):
                count += 1

        if include_start:
            count += 1
        if not include_end:
            count -= 1
        return count

    def compute_year_fraction(self,
                              start: datetime,
                              end: datetime,
                              include_start: bool = False,
                              include_end: bool = True):
        days_interval = self.compute_days_interval(start, end, include_start, include_end)
        return days_interval / self._days_in_year
