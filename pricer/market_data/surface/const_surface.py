from abc import ABC
from datetime import datetime
from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.surface.surface import Surface


class ConstantVolSurface(Surface, ABC):
    def __init__(self,
                 vol: float,
                 val_date: datetime,
                 business_time: BusinessTime):
        self._vol = vol
        self._val_date = val_date
        self._business_time = business_time

    def volatility(self, exp_date: datetime):
        if exp_date <= self._val_date:
            raise ValueError("ConstantVolSurface: exp_date should be later than surface-date when calc volatility")
        return self._vol

    def variance(self, exp_date: datetime):
        if exp_date <= self._val_date:
            raise ValueError("ConstantVolSurface: exp_date should be later than surface-date when calc variance")
        tau = self._business_time.compute_year_fraction(self._val_date, exp_date)
        return self._vol ** 2 * tau

    def get_business_time(self):
        return self._business_time

    def get_val_date(self) -> datetime:
        return self._val_date

    def bump(self, bump_size: float):
        return ConstantVolSurface(self._vol + bump_size, self._val_date, self._business_time)

    def relative_bump(self, bump_size: float):
        return ConstantVolSurface(self._vol * (1 + bump_size), self._val_date, self._business_time)

    def rolling(self, new_valuation_date: datetime):
        return ConstantVolSurface(self._vol, new_valuation_date, self._business_time)
