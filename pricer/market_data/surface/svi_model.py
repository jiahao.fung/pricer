from math import sqrt
from typing import List


class SVIModel(object):
    def __init__(self,
                 a: float,
                 b: float,
                 rho: float,
                 m: float,
                 sigma: float):
        self._a = a
        self._b = b
        self._rho = rho
        self._m = m
        self._sigma = sigma

    @property
    def a(self):
        return self._a

    @property
    def b(self):
        return self._b

    @property
    def rho(self):
        return self._rho

    @property
    def m(self):
        return self._m

    @property
    def sigma(self):
        return self._sigma

    def svi_variance(self, x):
        return self.a + self.b * (self.rho * (x-self.m) + sqrt((x-self.m) ** 2 + self.sigma ** 2))

    def calculate_jump_wing_params(self, tau) -> List[float]:
        """
        Calculate the jump-wing SVI parameter from svi raw
        :param tau:
        :return: [v, phi, p, c, v_hat]
        """
        wt = self.svi_variance(0)
        v = wt / tau
        phi = sqrt(1 / wt) * 2 / self.b * (-self.m / sqrt(self.m ** 2 + self.rho ** 2) + self.rho)
        p = sqrt(1 / wt) * self.b * (1 - self.rho)
        c = sqrt(1 / wt) * self.b * (1 + self.rho)
        v_hat = (self.a + self.b * self.rho * sqrt(1 - self.rho ** 2)) / tau
        return [v, phi, p, c, v_hat]
