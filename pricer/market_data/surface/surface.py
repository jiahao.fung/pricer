from abc import ABC, abstractmethod
from datetime import datetime

from pricer.market_data.holidays.business_time import BusinessTime


class Surface(ABC):
    @abstractmethod
    def volatility(self, *args):
        raise NotImplemented

    @abstractmethod
    def variance(self, *args):
        raise NotImplemented

    @abstractmethod
    def get_business_time(self) -> BusinessTime:
        raise NotImplemented

    @abstractmethod
    def get_val_date(self) -> datetime:
        raise NotImplemented

    @abstractmethod
    def bump(self, bump_size: float):
        raise NotImplemented

    @abstractmethod
    def relative_bump(self, bump_size: float):
        raise NotImplemented

    @abstractmethod
    def rolling(self, new_valuation_date: datetime):
        raise NotImplemented
