from abc import ABC
from datetime import datetime
from typing import List

from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.surface.surface import Surface
from pricer.market_data.surface.svi_model import SVIModel


class SVIVolSurface(Surface, ABC):
    def __init__(self,
                 val_date: datetime,
                 models: List[SVIModel],
                 taus: List[float],
                 business_time: BusinessTime):
        self._models = models
        self._taus = taus
        self._val_date = val_date
        self._business_time = business_time

    def volatility(self, x: float, exp_date: float):
        """
        :param x: strike in log-moneyness
        :return: volaitlity
        """
        raise NotImplemented

    def variance(self, x: float):
        raise NotImplemented

    def get_business_time(self) -> BusinessTime:
        return self._business_time

    def get_val_date(self) -> datetime:
        return self._val_date

    def bump(self, bump_size: float):
        raise NotImplemented

    def relative_bump(self, bump_size: float):
        raise NotImplemented

    def rolling(self, new_valuation_date: datetime):
        raise NotImplemented
