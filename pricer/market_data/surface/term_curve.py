import copy
from abc import ABC
from datetime import datetime
from math import sqrt
from typing import List
from scipy.interpolate import interp1d

from pricer.market_data.holidays.business_time import BusinessTime
from pricer.market_data.surface.surface import Surface


class TermVolSurface(Surface, ABC):
    def __init__(self,
                 val_date: datetime,
                 vols: List[float],
                 exp_dates: List[datetime],
                 business_time: BusinessTime):
        self._val_date = val_date
        self._business_time = business_time
        self._vols = vols
        self._exp_dates = exp_dates
        taus = [business_time.compute_year_fraction(self._val_date, exp_date) for exp_date in self._exp_dates]
        variances = []
        for i in range(len(vols)):
            variances.append(vols[i] ** 2 * taus[i])

        xs = copy.deepcopy(taus)
        xs.insert(0, 0.)
        ys = copy.deepcopy(variances)
        ys.insert(0, 0.)

        xs.insert(-1, xs[-1]+1)
        ys.insert(-1, self._vols[-1] ** 2 * xs[-1])

        self._interp = interp1d(x=xs, y=ys, kind='linear', bounds_error=False,
                                fill_value='extrapolate', assume_sorted=False)

    @classmethod
    def load_term_vol_from_dict(cls, val_date: datetime, vol_dict: dict, business_time: BusinessTime):
        vols = []
        exp_dates = []
        for key, value in vol_dict.items():
            exp_dates.append(key)
            vols.append(value)
        return TermVolSurface(val_date, vols, exp_dates, business_time)

    @classmethod
    def load_term_vol_from_tenor(cls):
        # TODO: add method for loading vol surface from tenor data
        pass

    def volatility(self, exp_date: datetime):
        if exp_date <= self._val_date:
            raise ValueError("TermVolSurface: exp_date should be later than surface-date when calc volatility")
        tau = self._business_time.compute_year_fraction(self._val_date, exp_date)
        var = self.variance(exp_date)
        return sqrt(var / tau)

    def variance(self, exp_date: datetime):
        if exp_date <= self._val_date:
            raise ValueError("TermVolSurface: exp_date should be later than surface-date when calc variance")
        tau = self._business_time.compute_year_fraction(self._val_date, exp_date)
        return float(self._interp(tau))

    def get_business_time(self) -> BusinessTime:
        return self._business_time

    def get_val_date(self) -> datetime:
        return self._val_date

    def bump(self, bump_size: float):
        bumped_vols = [vol + bump_size for vol in self._vols]
        return TermVolSurface(val_date=self._val_date,
                              vols=bumped_vols,
                              exp_dates=self._exp_dates,
                              business_time=self._business_time)

    def relative_bump(self, bump_size):
        bumped_vol = [vol * (1 + bump_size) for vol in self._vols]
        return TermVolSurface(val_date=self._val_date,
                              vols=bumped_vol,
                              exp_dates=self._exp_dates,
                              business_time=self._business_time)

    def rolling(self, new_valuation_date: datetime):
        vol_dict = {}
        cutoff_variance = self.variance(new_valuation_date)
        for i in range(len(self._exp_dates)):
            if self._exp_dates[i] > new_valuation_date:
                shifted_tau = self._business_time.compute_year_fraction(new_valuation_date, self._exp_dates[i])
                shifted_variance = self.variance(self._exp_dates[i]) - cutoff_variance
                vol_dict[self._exp_dates[i]] = sqrt(shifted_variance / shifted_tau)
        return TermVolSurface.load_term_vol_from_dict(new_valuation_date, vol_dict, self._business_time)

