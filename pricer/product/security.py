from pricer.product.instrument import Instrument


class EquityStock(Instrument):
    def __init__(self, stock_code: str):
        super().__init__(stock_code)

    def __str__(self):
        return f"EquityStock_{self.get_instrument_id()}"


class EquityIndex(Instrument):
    def __init__(self, index_code: str):
        super().__init__(index_code)

    def __str__(self):
        return f"EquityIndex_{self.get_instrument_id()}"


class Futures(Instrument):
    def __init__(self, futures_code: str):
        super().__init__(futures_code)

    def __str__(self):
        return f"Futures_{self.get_instrument_id()}"
