class Instrument:
    def __init__(self, instrument_id):
        self._instrument_id = instrument_id

    def get_instrument_id(self):
        return self._instrument_id
