from pricer.product.product_util import MktPriceType


class PriceLedger:
    def __init__(self,
                 ask: float = None,
                 bid: float = None,
                 close: float = None,
                 settle: float = None):
        self._ask = ask
        self._bid = bid
        self._close = close
        self._settle = settle

    def get_mkt_price(self, price_type: MktPriceType):
        if price_type == MktPriceType.Ask:
            return self._ask
        elif price_type == MktPriceType.Bid:
            return self._bid
        elif price_type == MktPriceType.Mid:
            return (self._ask + self._bid) / 2
        elif price_type == MktPriceType.Close:
            return self._close
        elif price_type == MktPriceType.Settle:
            return self._settle
        else:
            raise TypeError("Unsupported Type for PriceLedger")
