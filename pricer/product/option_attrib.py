from abc import ABC, abstractmethod
from datetime import datetime


class HasExpirationDate(object):
    def __init__(self, expiration_date: datetime):
        self._expiration_date = expiration_date

    @property
    def expiration_date(self):
        return self._expiration_date


class HasDeliveryDate(object):
    def __init__(self, delivery_date: datetime):
        self._delivery_date = delivery_date

    @property
    def delivery_date(self):
        return self._delivery_date


class Decomposable(ABC):
    @abstractmethod
    def decompose(self):
        raise NotImplemented
