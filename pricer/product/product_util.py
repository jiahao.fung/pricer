from enum import Enum


class OptionType(Enum):
    Call = 1
    Put = 2


class MktPriceType(Enum):
    Close = "Close"
    Bid = "Bid"
    Ask = "Ask"
    Settle = "Settle"
    Mid = "Mid"


class EuropeanOption:
    def __init__(self):
        pass


class AmericanOption:
    def __init__(self):
        pass
