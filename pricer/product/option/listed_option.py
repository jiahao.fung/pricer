from datetime import datetime
from pricer.product.instrument import Instrument
from pricer.product.ledger import PriceLedger
from pricer.product.option.vanilla_option import VanillaOption
from pricer.product.product_util import OptionType, EuropeanOption


class ListedOption(VanillaOption):
    def __init__(self, mkt_price_ledger: PriceLedger, expiration_date: datetime, underlying: Instrument,
                 option_type: OptionType, strike: float, delivery_date: datetime):
        VanillaOption.__init__(self, underlying, option_type, strike, expiration_date, delivery_date)
        self._mkt_price = mkt_price_ledger

    @property
    def mkt_price(self):
        return self._mkt_price


class ListedVanillaEuropean(ListedOption, EuropeanOption):
    def __init__(self, mkt_price_ledger: PriceLedger, expiration_date: datetime, underlying: Instrument,
                 option_type: OptionType, strike: float, delivery_date: datetime):
        super().__init__(mkt_price_ledger, expiration_date, underlying, option_type, strike, delivery_date)
