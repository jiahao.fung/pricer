from datetime import datetime

from pricer.product.instrument import Instrument
from pricer.product.option_attrib import HasExpirationDate, HasDeliveryDate
from pricer.product.product_util import OptionType, EuropeanOption, AmericanOption


class VanillaOption(HasExpirationDate, HasDeliveryDate, Instrument):
    def __init__(self,
                 underlying: Instrument,
                 option_type: OptionType,
                 strike: float,
                 expiration_date: datetime,
                 delivery_date: datetime):
        HasExpirationDate.__init__(self, expiration_date)
        HasDeliveryDate.__init__(self, delivery_date)
        self._underlying = underlying
        self._option_type = option_type
        self._strike = strike

    @property
    def strike(self) -> float:
        return self._strike

    @property
    def option_type(self) -> OptionType:
        return self._option_type

    @property
    def underlying(self) -> Instrument:
        return self._underlying


class VanillaEuropean(VanillaOption, EuropeanOption):
    def __init__(self, underlying: Instrument, option_type: OptionType, strike: float, expiration_date: datetime,
                 delivery_date: datetime):
        super().__init__(underlying, option_type, strike, expiration_date, delivery_date)


class VanillaAmerican(VanillaOption, AmericanOption):
    def __init__(self, underlying: Instrument, option_type: OptionType, strike: float, expiration_date: datetime,
                 delivery_date: datetime):
        super().__init__(underlying, option_type, strike, expiration_date, delivery_date)
