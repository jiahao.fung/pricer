from math import exp, log, sqrt
from scipy.stats import norm

from pricer.product.product_util import OptionType


class BSVanillaEuropean:

    @staticmethod
    def calc_d1(spot, strike, vol, tau, r, q):
        return 1. / (vol * sqrt(tau)) * (log(spot / strike) + (r - q + .5 * vol ** 2) * tau)

    @staticmethod
    def pv(spot: float, strike: float, vol: float, r: float,
           q: float, tau: float, option_type: OptionType,
           r_delivery: float, tau_delivery: float) -> float:
        if tau < 1e-4:
            return max(0., spot - strike) if option_type == OptionType.Call else max(0., strike - spot)
        d1 = BSVanillaEuropean.calc_d1(spot, strike, vol, tau, r, q)
        d2 = d1 - vol * sqrt(tau)
        extra_df = exp(-r_delivery * tau_delivery + r * tau)
        if option_type == OptionType.Call:
            price = norm.cdf(d1) * spot * exp(-q * tau) - norm.cdf(d2) * strike * exp(-r * tau)
        else:
            price = norm.cdf(-d2) * strike * exp(-r * tau) - norm.cdf(-d1) * spot * exp(-q * tau)
        return price * extra_df
