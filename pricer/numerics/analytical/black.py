from math import exp, log, sqrt
from scipy.stats import norm

from pricer.product.product_util import OptionType


class BlackVanillaEuropean:

    @staticmethod
    def calc_d1(forward, strike, vol, tau):
        return 1. / (vol * sqrt(tau)) * (log(forward / strike) + (.5 * vol ** 2) * tau)

    @staticmethod
    def pv(forward: float, strike: float, vol: float, r: float,
           tau: float, option_type: OptionType,
           r_delivery: float, tau_delivery: float) -> float:
        if tau < 1e-4:
            return max(0., forward - strike) if option_type == OptionType.Call else max(0., strike - forward)
        d1 = BlackVanillaEuropean.calc_d1(forward, strike, vol, tau)
        d2 = d1 - vol * sqrt(tau)
        extra_df = exp(-r_delivery * tau_delivery + r * tau)
        if option_type == OptionType.Call:
            price = (norm.cdf(d1) * forward - norm.cdf(d2) * strike) * exp(-r * tau)
        else:
            price = (norm.cdf(-d2) * strike - norm.cdf(-d1) * forward) * exp(-r * tau)
        return price * extra_df
