from scipy import optimize
from typing import List

from pricer.interface.pricing_engine import option_calculator
from pricer.interface.pricing_environment import PricingEnvironment
from pricer.interface.pricing_measure import PricingMeasure
from pricer.market_data.surface.const_surface import ConstantVolSurface
from pricer.product.option.listed_option import ListedOption
from pricer.product.product_util import MktPriceType


def calc_implied_vol(mkt_price: float, list_option: ListedOption, pricing_env: PricingEnvironment):
    def target_f(vol) -> float:
        vol_surface_ = ConstantVolSurface(vol,
                                          pricing_env.vol_surface.get_val_date(),
                                          pricing_env.vol_surface.get_business_time())
        pricing_env.vol_surface = vol_surface_
        return option_calculator(PricingMeasure.PV, list_option, pricing_env) - mkt_price
    return optimize.brentq(target_f, 1e-4, 10)


class ImpliedVolSolver(object):
    def __init__(self,
                 listed_options: List[ListedOption],
                 pricing_env: PricingEnvironment):
        self.listed_options = listed_options
        self.pricing_env = pricing_env

    def solve_implied_vol(self, quote_type: MktPriceType):
        implied_vols = {}
        for listed_option in self.listed_options:
            iv = calc_implied_vol(listed_option.mkt_price.get_mkt_price(quote_type),
                                  listed_option, self.pricing_env)
            if listed_option.expiration_date not in implied_vols:
                implied_vols[listed_option.expiration_date] = {}
            implied_vols[listed_option.expiration_date][listed_option.strike] = iv
        return implied_vols
