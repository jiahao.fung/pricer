import unittest


if __name__ == "__main__":
    test_dir = "./tests"
    discover = unittest.defaultTestLoader.discover("./tests", pattern="test_*.py")
    runner = unittest.TextTestRunner()
    runner.run(discover)
